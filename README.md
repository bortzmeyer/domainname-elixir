# DomainName

**A module to describe Internet domain names, together with some
useful functions (such as finding if a domain is the subdomain of another).**

This module does not implement the DNS protocol, it its only for
domain names, independently of how they are used. 

It was mostly created for the [Drink
server](https://framagit.org/bortzmeyer/drink) (see its [ticket
#45](https://framagit.org/bortzmeyer/drink/-/issues/45)) so it is not
sure it is generally useful. Try if you want.

## Installation

The package is [available in Hex](https://hex.pm/docs/publish), it can be installed
by adding `domainname` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:domainname, "~> 0.1.5"}
  ]
end
```

## Future work

See the [list of
issues](https://framagit.org/bortzmeyer/domainname-elixir/-/issues).

  
