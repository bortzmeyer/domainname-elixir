defmodule DomainName.MixProject do
  use Mix.Project

  def project do
    [
      app: :domainname,
      description: "Library to handle domain names (no DNS, just the names)",
      version: "0.1.5",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      source_url: "https://framagit.org/bortzmeyer/domainname-elixir",
      homepage_url: "https://hex.pm/packages/domainname",
      package: [
        licenses: ["BSD-3-Clause"],
        links: %{
          "Forge" => "https://framagit.org/bortzmeyer/domainname-elixir",
          "Documentation" => "http://hexdocs.pm/domainname"
        }
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    []
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # `mix docs` to generate doc
      {:ex_doc, "~> 0.27", only: :dev, runtime: false},
      # For Dialyzer static analysis https://hexdocs.pm/dialyxir/
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false}
    ]
  end
end
