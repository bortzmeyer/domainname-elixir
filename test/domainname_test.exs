defmodule DomainNameTest do
  use ExUnit.Case
  doctest DomainName

  setup do
    # tld and label must be in uppercase or mixedcase, at least one
    # test depend on it.
    tld = "Example"
    # Must be different from tld
    other_tld = "com"
    label = "fooBAR"
    domain = label <> "." <> tld
    end_many_labels = "thing.example"
    begin_many_labels_orig = "fOo.BAR.stuff"
    begin_many_labels = String.downcase(begin_many_labels_orig)
    many_labels_orig = "#{begin_many_labels_orig}.#{end_many_labels}"
    many_labels = "#{begin_many_labels}.#{end_many_labels}"
    # You have to do the counting yourself
    num_many_labels = 5
    # The next two have to be different from domain
    other_domain = "foobar.com"
    yet_other_domain = "FOObar.org"

    [
      regular: String.downcase(domain),
      regular_fqdn: String.downcase(domain) <> ".",
      regular_mixed_case: domain,
      regular_labels: [String.downcase(label), String.downcase(tld)],
      regular_mixed_labels: [label, tld],
      other_regular: other_domain,
      yet_other_regular: yet_other_domain,
      ldh: "machin-chose-098.fr",
      many_labels_orig: many_labels_orig,
      many_labels: many_labels,
      begin_many_labels_orig: begin_many_labels_orig,
      begin_many_labels: begin_many_labels,
      end_many_labels: end_many_labels,
      num_labels: num_many_labels,
      nohost: "machin fr$",
      tld: String.downcase(tld),
      other_tld: other_tld,
      twodots: "machin..fr",
      idn: "café.fr",
      punycode: "xn--caf-dma.fr",
      charlist: 'foo.bar.example'
    ]
  end

  test "new1", fixture do
    assert {:ok, %DomainName{}} = DomainName.new(fixture.regular)
  end

  test "new2", fixture do
    assert {:ok, %DomainName{}} = DomainName.new(fixture.nohost)
  end

  test "new3", fixture do
    assert {:error, _reason} = DomainName.new(fixture.nohost, must_be_hostname: true)
  end

  test "new4", fixture do
    assert {:error, _reason} = DomainName.new(fixture.twodots, must_be_hostname: true)
  end

  test "new5", fixture do
    assert {:ok, %DomainName{}} = DomainName.new(fixture.tld)
  end

  test "new6" do
    assert {:error, _reason} = DomainName.new("")
  end

  test "new7" do
    assert_raise(FunctionClauseError, fn -> DomainName.new(nil) end)
  end

  test "new8" do
    assert {:ok, %DomainName{}} = DomainName.new(".")
  end

  # IDN not supported yet, see issue #2
  test "new9", fixture do
    assert {:error, _reason} = DomainName.new(fixture.idn)
  end

  # But Punycode must be OK
  test "new10", fixture do
    assert {:ok, %DomainName{}} = DomainName.new(fixture.punycode)
  end

  test "new11", fixture do
    assert {:ok, _domain} = DomainName.new(fixture.nohost, must_be_hostname: false)
  end

  test "new!1", fixture do
    assert %DomainName{} = DomainName.new!(fixture.regular)
  end

  test "new!2", fixture do
    assert_raise(DomainName.Invalid, fn ->
      DomainName.new!(fixture.nohost, must_be_hostname: true)
    end)
  end

  test "new!3", fixture do
    assert %DomainName{} = DomainName.new!(fixture.regular, must_be_hostname: true)
  end

  test "new!4" do
    assert {:error, _reason} = DomainName.new(".exemple.fr")
  end

  # We accept charlists, too
  test "new!5", fixture do
    assert {:ok, %DomainName{}} = DomainName.new(fixture.charlist)
  end

  test "extract1", fixture do
    assert DomainName.name(DomainName.new!(fixture.regular)) == fixture.regular
  end

  test "extract2", fixture do
    assert DomainName.name(DomainName.new!(fixture.regular_mixed_case)) == fixture.regular
  end

  test "hostname1", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.regular)
    assert DomainName.can_be_hostname?(d)
  end

  test "hostname2", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.nohost)
    assert not DomainName.can_be_hostname?(d)
  end

  test "hostname3", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.ldh)
    assert DomainName.can_be_hostname?(d)
  end

  test "labels1", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.regular)
    assert DomainName.labels(d) == fixture.regular_labels
  end

  test "labels2", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.regular_fqdn)
    assert DomainName.labels(d) == fixture.regular_labels
  end

  test "labels3", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.tld)
    assert DomainName.labels(d) == [fixture.tld]
  end

  test "labels4", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.regular_mixed_case)
    assert DomainName.labels(d) == fixture.regular_labels
  end

  test "labels5", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.regular_mixed_case)
    assert DomainName.original_labels(d) == fixture.regular_mixed_labels
  end

  test "labels6", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.many_labels)
    assert Enum.count(DomainName.labels(d)) == fixture.num_labels
  end

  test "tld1", fixture do
    {:ok, d = %DomainName{}} = DomainName.new(fixture.regular)
    assert DomainName.tld(d) == fixture.tld
  end

  test "equal1", fixture do
    {:ok, l = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, r = %DomainName{}} = DomainName.new(fixture.regular)
    assert DomainName.equal?(l, r)
  end

  test "equal2", fixture do
    {:ok, l = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, r = %DomainName{}} = DomainName.new(fixture.regular_fqdn)
    assert DomainName.equal?(l, r)
  end

  test "equal3", fixture do
    {:ok, l = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, r = %DomainName{}} = DomainName.new(fixture.other_regular)
    assert not DomainName.equal?(l, r)
  end

  test "equal4", fixture do
    {:ok, l = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, r = %DomainName{}} = DomainName.new("www.#{fixture.regular}")
    assert not DomainName.equal?(l, r)
  end

  test "equal5", fixture do
    {:ok, s = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, g1 = %DomainName{}} = DomainName.new(fixture.regular_mixed_case)
    {:ok, g2 = %DomainName{}} = DomainName.new(fixture.other_regular)
    assert {true, _g} = DomainName.is_one_of(s, [g1, g2])
  end

  test "equal6", fixture do
    {:ok, s = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, g1 = %DomainName{}} = DomainName.new(fixture.yet_other_regular)
    {:ok, g2 = %DomainName{}} = DomainName.new(fixture.other_regular)
    assert not DomainName.is_one_of(s, [g1, g2])
  end

  test "ends1", fixture do
    {:ok, s = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, g = %DomainName{}} = DomainName.new(String.upcase(fixture.tld))
    assert DomainName.ends_with?(s, g)
  end

  test "ends2", fixture do
    {:ok, s = %DomainName{}} = DomainName.new(fixture.tld)
    {:ok, g = %DomainName{}} = DomainName.new(String.upcase(fixture.tld))
    assert DomainName.ends_with?(s, g)
  end

  test "ends3", fixture do
    {:ok, s = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, g1 = %DomainName{}} = DomainName.new(String.upcase(fixture.tld))
    {:ok, g2 = %DomainName{}} = DomainName.new(fixture.other_tld)
    assert {true, _g} = DomainName.ends_with(s, [g1, g2])
  end

  test "ends4", fixture do
    {:ok, s = %DomainName{}} = DomainName.new(fixture.yet_other_regular)
    {:ok, g1 = %DomainName{}} = DomainName.new(fixture.tld)
    {:ok, g2 = %DomainName{}} = DomainName.new(fixture.other_tld)
    assert not DomainName.ends_with(s, [g1, g2])
  end

  test "ends5", fixture do
    {:ok, s = %DomainName{}} = DomainName.new(fixture.regular)
    # Everything is a subdomain of the root
    {:ok, g = %DomainName{}} = DomainName.new(".")
    assert DomainName.ends_with?(s, g)
  end

  test "ends6", fixture do
    {:ok, s = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, g1 = %DomainName{}} = DomainName.new(String.upcase(fixture.tld))
    {:ok, g2 = %DomainName{}} = DomainName.new(fixture.other_tld)
    assert DomainName.ends_with?(s, [g1, g2])
  end

  test "ends7", fixture do
    {:ok, s = %DomainName{}} = DomainName.new(fixture.yet_other_regular)
    {:ok, g1 = %DomainName{}} = DomainName.new(fixture.tld)
    {:ok, g2 = %DomainName{}} = DomainName.new(fixture.other_tld)
    assert not DomainName.ends_with?(s, [g1, g2])
  end

  test "suffix1", fixture do
    {:ok, long = %DomainName{}} = DomainName.new(fixture.many_labels)
    {:ok, short} = DomainName.without_suffix(long, DomainName.new!(fixture.end_many_labels))
    assert "#{short}" == fixture.begin_many_labels
  end

  test "suffix2", fixture do
    {:ok, long = %DomainName{}} = DomainName.new(fixture.many_labels)
    {:error, long} = DomainName.without_suffix(long, DomainName.new!(fixture.regular))
    assert "#{long}" == fixture.many_labels
  end

  test "suffix3", fixture do
    {:ok, base = %DomainName{}} = DomainName.new(fixture.regular)
    {:ok, empty} = DomainName.without_suffix(base, base)
    assert DomainName.labels(empty) == []
  end

  test "suffix4", fixture do
    {:ok, long = %DomainName{}} = DomainName.new(fixture.many_labels_orig)
    {:ok, short} = DomainName.without_suffix(long, DomainName.new!(fixture.end_many_labels))

    assert "#{short}" == fixture.begin_many_labels and
             DomainName.original(short) == fixture.begin_many_labels_orig
  end

  test "original1", fixture do
    assert DomainName.original(DomainName.new!(fixture.regular)) == fixture.regular
  end

  test "original2", fixture do
    assert DomainName.original(DomainName.new!(fixture.regular_mixed_case)) ==
             fixture.regular_mixed_case
  end

  test "original3", fixture do
    assert DomainName.original(DomainName.new!(fixture.regular_mixed_case)) != fixture.regular
  end

  test "join1", fixture do
    sx = DomainName.new!(fixture.regular)
    assert DomainName.name(DomainName.join!("www", sx)) == "www.#{fixture.regular}"
  end

  test "join2", fixture do
    d = DomainName.new!("www.#{fixture.regular}")
    assert DomainName.equal?(DomainName.join!(DomainName.labels(d)), d)
  end

  test "join3", fixture do
    sx = DomainName.new!(fixture.regular)
    assert DomainName.name(DomainName.join!("www.sub", sx)) == "www.sub.#{fixture.regular}"
  end

  test "join4" do
    assert DomainName.name(DomainName.join!(["www", "machin", "fr"])) == "www.machin.fr"
  end

  test "encode1" do
    d = DomainName.new!("toc")
    assert DomainName.encode(d) == [3, 116, 111, 99, 0]
  end

  test "encode2" do
    assert DomainName.encode(DomainName.new!("foo.test")) == [
             3,
             102,
             111,
             111,
             4,
             116,
             101,
             115,
             116,
             0
           ]
  end

  test "encode3" do
    assert DomainName.encode(DomainName.new!("barzig")) == [6, 98, 97, 114, 122, 105, 103, 0]
  end

  test "encode4" do
    assert DomainName.encode(DomainName.new!(".")) == [0]
  end

  test "encode5" do
    d = DomainName.new!("tOc")
    assert DomainName.encode(d, original: true) == [3, 116, 79, 99, 0]
  end

  test "encode6" do
    d = DomainName.new!("tOc")
    assert DomainName.encode(d, original: false) == [3, 116, 111, 99, 0]
  end

  test "encode7" do
    d = DomainName.new!("tOc")
    assert DomainName.encode(d) == [3, 116, 111, 99, 0]
  end

  test "decode1" do
    {:ok, d} = DomainName.decode([3, 116, 111, 99, 0])
    assert DomainName.name(d) == "toc"
  end

  # No root at the end
  test "decode2" do
    assert {:error, _reason} = DomainName.decode([3, 104, 107, 103])
  end

  # Invalid length of the first label
  test "decode3" do
    assert {:error, _reason} = DomainName.decode([8, 104, 107, 103, 0])
  end

  test "decode!1" do
    assert DomainName.name(DomainName.decode!([3, 116, 111, 99, 0])) == "toc"
  end

  # No root at the end
  test "decode!2" do
    assert_raise DomainName.Invalid, fn -> DomainName.decode!([3, 164, 157, 143]) end
  end

  # Invalid length of the first label
  test "decode!3" do
    assert_raise DomainName.Invalid, fn -> DomainName.decode!([8, 164, 157, 143, 0]) end
  end

  test "tostring1", fixture do
    d = DomainName.new!(fixture.regular_mixed_case)
    assert "#{d}" == fixture.regular
  end

  test "tocharlist1", fixture do
    d = DomainName.new!(fixture.regular_mixed_case)
    assert to_charlist(d) == to_charlist(fixture.regular)
  end

  test "inspect1", fixture do
    d = DomainName.new!(fixture.regular)
    assert inspect(d) == "#DomainName<#{fixture.regular}>"
  end

  test "inspect2", fixture do
    d = DomainName.new!(fixture.regular_mixed_case)
    assert inspect(d) == "#DomainName<#{fixture.regular}>"
  end
end
